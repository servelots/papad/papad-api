# Generated by Django 4.0.4 on 2022-08-06 21:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("common", "0006_question_group_extra_group_questions"),
    ]

    operations = [
        migrations.AddField(
            model_name="group",
            name="delete_wait_for",
            field=models.IntegerField(default=0, verbose_name="Wait before delete"),
        ),
    ]
