from django.contrib import admin

from papadapi.common.models import Group, Tags


class BaseAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False

    def has_change_permission(self, request, obj=None):
        # Disable delete
        return False

    def get_uuid_formatted(self, obj):
        if obj:
            return str(obj.uuid)

    get_uuid_formatted.admin_order_field = "uuid"
    get_uuid_formatted.short_description = "UUID"


class GroupAdmin(BaseAdmin):
    list_display = ("name", "is_active", "is_public", "created_at", "updated_at")
    list_filter = ("is_active", "is_public")


class TagAdmin(BaseAdmin):
    list_display = ("name", "count", "created_at", "updated_at")


admin.site.register(Group, GroupAdmin)
admin.site.register(Tags, TagAdmin)
