import os

from environs import Env

from .common import Common

env = Env()
env.read_env("service_config.env", recurse=False)


class Production(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    # Site
    # https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts
    INSTALLED_APPS += ("gunicorn",)
    APP_ENV = env.str("DJANGO_APP_ENV", "Production")
    APP_HTTPS = env.bool("DJANGO_APP_HTTPS", "Production")
    APP_URL = env.str("API_URL", None)
    if APP_ENV == "production":
        DEBUG = False
        ALLOWED_HOSTS = [APP_URL]
        if APP_HTTPS:
            SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
            CSRF_TRUSTED_ORIGINS = ["https://"+APP_URL]
        else:
            SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'http')
            CSRF_TRUSTED_ORIGINS = [APP_URL]

