from rest_framework.permissions import SAFE_METHODS, BasePermission

from papadapi.common.models import Group


class IsArchiveCreateOrReadOnly(BasePermission):
    message = "You are not a member of the group to perform this action"

    def has_permission(self, request, view):
        # Always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS:
            return True
        # User must be a part of the group.

        data = request.data
        group_id = data["group"]
        user = request.user
        if group_id and user:
            group = Group.objects.get(id=group_id)
            if user in group.users.all():
                return True
            else:
                return False
        else:
            self.message = "User or Group detail missing"
            return False


class IsArchiveCopyAllowed(BasePermission):
    message = "You are not a member of the group to perform this action"

    def has_permission(self, request, view):
        if request.method == "PUT":
            data = request.data
            from_group_id = data["from_group"]
            to_group_id = data["to_group"]
            user = request.user
            if from_group_id and to_group_id and user:
                group = Group.objects.get(id=to_group_id)
                if user in group.users.all():
                    return True
                else:
                    return False
            else:
                self.message = "User or Group detail missing"
                return False
        else:
            self.message = "Only POST method allowed"
            return False


class IsArchiveUpdateOrReadOnly(BasePermission):
    message = "You are not a member of the group to perform this action"

    def has_object_permission(self, request, view, obj):
        # Always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS:
            return True
        # User must be a part of the group.
        archive_instance = obj
        group = obj.group
        user = request.user
        if group and user:
            if user in group.users.all():
                return True
            else:
                return False
        else:
            self.message = "User or Group detail missing"
            return False
