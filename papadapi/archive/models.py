import hashlib
import os
import uuid
from functools import partial

from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.translation import gettext as _

from djrichtextfield.models import RichTextField

from papadapi.common.models import Tags


def hash_file(file, block_size=65536):
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b""):
        hasher.update(buf)
    return hasher.hexdigest()


def upload_to(instance, filename):
    """
    :type instance: dolphin.models.File
    """
    instance.upload.open()
    filename_base, filename_ext = os.path.splitext(filename)

    return "archive/{}{}".format(hash_file(instance.upload), filename_ext)


class MediaStore(models.Model):
    upload = models.FileField(
        _("Uploaded Archive"), max_length=100, upload_to=upload_to
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(_("Name"), max_length=300)
    description = RichTextField(_("Description"))
    tags = models.ManyToManyField("common.Tags", verbose_name=_("tags"))
    is_public = models.BooleanField(_("Public"), default=True)
    group = models.ForeignKey(
        "common.Group",
        verbose_name=_("Group belonging to"),
        on_delete=models.CASCADE,
        default=1,
    )
    extra_group_response = models.JSONField(blank=True, null=True, default=[])
    is_delete = models.BooleanField(_("User requested soft delete ?"), default=False)
    is_instance_admin_withheld = models.BooleanField(
        _("withheld by instance admin?"), default=False
    )
    is_instance_group_withheld = models.BooleanField(
        _("withheld by group admin?"), default=False
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        "users.User",
        verbose_name=_("Who created the media"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("MediaStore")
        verbose_name_plural = _("MediaStores")

    def __str__(self):
        return str(self.uuid)

    def get_absolute_url(self):
        return reverse("MediaStore_detail", kwargs={"pk": self.pk})
