from huey.contrib.djhuey import db_task

from papadapi.archive.models import MediaStore


@db_task()
def delete_media_post_schedule(media_id):
    media = MediaStore.objects.get(id=media_id)
    media.delete()
