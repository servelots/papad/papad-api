# Generated by Django 4.0.4 on 2022-04-27 18:32

from django.db import migrations, models

import papadapi.archive.models


class Migration(migrations.Migration):

    dependencies = [
        ("archive", "0003_delete_tags_alter_mediastore_tags"),
    ]

    operations = [
        migrations.AddField(
            model_name="mediastore",
            name="uuid",
            field=models.UUIDField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="mediastore",
            name="upload",
            field=models.FileField(
                upload_to=papadapi.archive.models.upload_to,
                verbose_name="Uploaded Archive",
            ),
        ),
    ]
