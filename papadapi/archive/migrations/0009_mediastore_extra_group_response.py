# Generated by Django 4.0.4 on 2022-07-14 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("archive", "0008_mediastore_group"),
    ]

    operations = [
        migrations.AddField(
            model_name="mediastore",
            name="extra_group_response",
            field=models.JSONField(blank=True, default={}, null=True),
        ),
    ]
