from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path, reverse_lazy

# from django.contrib import admin
from django.views.generic.base import RedirectView
from rest_framework.authtoken import views
from rest_framework.routers import DefaultRouter, SimpleRouter

from papadapi.archive.views import (
    GroupMediaStats,
    InstanceMediaStats,
    MediaStoreAddTag,
    MediaStoreRemoveTag,
)

# Common Imports
from papadapi.common.views import (
    AddCustomQuestionFromGroupView,
    GroupViewSet,
    InstanceGroupStats,
    RemoveCustomQuestionFromGroupView,
    RemoveUserFromGroupView,
    TagsViewSet,
    UpdateCustomQuestionFromGroupView,
    UpdateGroupViewSet,
    GroupTagGraphView,
)
from papadapi.importexport.views import (
    ExportGroupCreateListSet,
    ImportExportGroupViewSet,
    ImportGroupCreateSet,
    UserImportExportViewer,
)

# Annotate Imports
from .annotate.views import (
    AnnotationByMediaRetreiveSet,
    AnnotationCreateSet,
    AnnotationRetreiveSet,
    AnnotationAddTag,
    AnnotationRemoveTag,
    GroupAnnotationStats,
    InstanceAnnotationStats,
)

# Archive Imports
from .archive.views import MediaStoreCopySet, MediaStoreCreateSet, MediaStoreUpdateSet

# User Imports
from .users.views import InstanceUserStats, SearchUserView

# Import Export Views



router = DefaultRouter(trailing_slash=True)
router.register(r"users/search", SearchUserView)
router.register(r"archive", MediaStoreUpdateSet)
router.register(r"archive", MediaStoreCreateSet)
router.register(r"archive/copy", MediaStoreCopySet)
router.register(r"archive/remove_tag", MediaStoreRemoveTag)
router.register(r"archive/add_tag", MediaStoreAddTag)
router.register(r"tags", TagsViewSet)
router.register(r"group", GroupViewSet)
router.register(r"group", UpdateGroupViewSet)
router.register(r"group/remove_user", RemoveUserFromGroupView)
router.register(r"group/remove_question", RemoveCustomQuestionFromGroupView)
router.register(r"group/add_question", AddCustomQuestionFromGroupView)
router.register(r"group/update_question", UpdateCustomQuestionFromGroupView)
router.register(r"annotate", AnnotationCreateSet)
router.register(r"annotate", AnnotationRetreiveSet)
router.register(r"annotate/search", AnnotationByMediaRetreiveSet)
router.register(r"annotate/remove_tag", AnnotationRemoveTag)
router.register(r"annotate/add_tag", AnnotationAddTag)
router.register(r"export", ExportGroupCreateListSet)
router.register(r"export", ImportExportGroupViewSet)
router.register(r"import", ImportGroupCreateSet)
router.register(r"import", ImportExportGroupViewSet)
router.register(r"myierequests", UserImportExportViewer)
router.register(r"instancegroupstats", InstanceGroupStats)
router.register(r"instanceuserstats", InstanceUserStats)
router.register(r"instancemediastats", InstanceMediaStats)
router.register(r"instanceannotationstats", InstanceAnnotationStats)
router.register(r"groupannotationstats", GroupAnnotationStats)
router.register(r"groupmediastats", GroupMediaStats)
router.register(r"group/taggraph", GroupTagGraphView)


urlpatterns = [
    # path('admin/', admin.site.urls),
    path("api/v1/", include(router.urls)),
    re_path(r"^auth/", include("djoser.urls")),
    re_path(r"^auth/", include("djoser.urls.authtoken")),
    path("nimda/", admin.site.urls),
    # the 'api-root' from django rest-frameworks default router
    # http://www.django-rest-framework.org/api-guide/routers/#defaultrouter
    re_path(r"^$", RedirectView.as_view(url=reverse_lazy("api-root"), permanent=False)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
