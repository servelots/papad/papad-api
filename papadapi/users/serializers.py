from rest_framework import serializers

from papadapi.common.models import Group

from .models import User

# class MenuSerializer(serializers.ModelSerializer):
#     users = serializers.SerializerMethodField()

#     class Meta:
#         model = Menu
#         fields = ['id', 'food_name', 'users']

#     def get_users(self, obj):
#         users = User.objects.filter(pk__in=obj.order_set.all().values('user'))
#         return UserSerializer(users, many=True).data


# class OrderSerializer(serializers.HyperlinkedModelSerializer):
#     menu = MenuSerializer()
#     class Meta:
#         model = Order
#         fields = ['id', 'menu']


class UsersAPIGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            "id",
            "name",
            "description",
            "is_public",
            "created_at",
            "updated_at"
        )


class UserMEApiSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name", "is_superuser", "groups")
        read_only_fields = ("username",)

    def get_groups(self, obj):
        groups_user_in = Group.objects.filter(users__in=[obj])
        return UsersAPIGroupSerializer(groups_user_in, many=True).data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "first_name", "last_name")
        read_only_fields = ("username",)


class UserStatsSerializer(serializers.Serializer):
    created_date = serializers.DateField()
    total = serializers.IntegerField()
