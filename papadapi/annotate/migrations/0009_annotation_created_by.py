# Generated by Django 4.0.6 on 2022-09-01 07:35

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("annotate", "0008_annotation_is_instance_admin_withheld_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="annotation",
            name="created_by",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
                verbose_name="Who created the annotation",
            ),
        ),
    ]
