# Generated by Django 4.0.6 on 2022-08-31 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("annotate", "0007_annotation_is_delete"),
    ]

    operations = [
        migrations.AddField(
            model_name="annotation",
            name="is_instance_admin_withheld",
            field=models.BooleanField(
                default=False, verbose_name="withheld by instance admin?"
            ),
        ),
        migrations.AddField(
            model_name="annotation",
            name="is_instance_group_withheld",
            field=models.BooleanField(
                default=False, verbose_name="withheld by group admin?"
            ),
        ),
    ]
