from rest_framework.permissions import SAFE_METHODS, BasePermission

from papadapi.archive.models import MediaStore
from papadapi.common.models import Group


class IsAnnotateCreateOrReadOnly(BasePermission):
    message = "You are not a member of the group to perform this action"

    def has_permission(self, request, view):
        # Always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS:
            return True
        # User must be a part of the group.

        data = request.data
        archive_id = data["media_reference_id"]
        archive = MediaStore.objects.get(uuid=archive_id)
        group = None
        if archive:
            group = archive.group
        user = request.user
        if group and user:
            if user in group.users.all():
                return True
            else:
                return False
        else:
            self.message = "User or Group detail missing"
            return False


class IsAnnotateUpdateOrReadOnly(BasePermission):
    message = "You are not a member of the group to perform this action"

    def has_object_permission(self, request, view, obj):
        # Always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS:
            return True
        # User must be a part of the group.

        archive_instance = obj
        archive = MediaStore.objects.get(uuid=obj.media_reference_id)
        group = archive.group
        user = request.user
        if group and user:
            if user in group.users.all():
                return True
            else:
                return False
        else:
            self.message = "User or Group detail missing"
            return False
