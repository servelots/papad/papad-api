FROM python:3.8-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --update --no-cache gcc musl-dev libffi-dev libc-dev bash
# Adds our application app to the image
COPY . /app/
WORKDIR /app/
RUN pip install -r requirements-prod.txt

EXPOSE 8000
