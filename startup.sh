python manage-prod.py migrate
python manage-prod.py collectstatic --noinput
gunicorn --bind 0.0.0.0:$PORT --forwarded-allow-ips="*" --workers=2 --access-logfile - papadapi.wsgi:application
